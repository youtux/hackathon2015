# -*- coding: utf-8 -*-
import os
import json
from xml.etree import ElementTree
from datetime import datetime, timedelta
from collections import OrderedDict

from bottle import *
import requests
from repoze.lru import lru_cache

now = datetime.now


CIVIS_BASE_URL = "http://civis.cloud.reply.eu/Civis/EnergyPlatform.svc"

CIVIS_TIME_FORMAT = "%m/%d/%Y %I:%M:%S %p"


@lru_cache(maxsize=200)
def fetch_token_for_user(user_id):
    user_id = int(user_id)
    response = requests.get(CIVIS_BASE_URL + "/getAllUsers")
    rtree = ElementTree.fromstring(response.content)

    for userID, token in rtree:
        if int(userID.text) != user_id:
            continue
        return token.text

def check_credentials(user_id, token):
    try:
        actual_token = fetch_token_for_user(user_id)
    except ValueError:
        return False

    if token != actual_token:
        return False

def civis_format_time(time):
    return time.strftime("%d-%b-%y %H:%M:%S")

@get('/api/charges/<city>')
def charges(city):
    with open("data/charges.json") as f:
        data = json.load(f)
    print data
    return json.dumps({'data': data}, ensure_ascii=False)


# Authentication needed, I know. Sorry. No time.
@get('/api/plantData')
def plant_data():
    user_id, token = request.headers['userID'], request.headers['token']

    # if not check_credentials(user_id, token):
    #     abort(401, "Sorry, access denied.")

    interval = request.params['interval']

    # print user_id, token, interval
    response_header_0_map = {
        'now': 'Minute',
        'last_day': "Hour",
        'last_week': "Day",
        'last_month': "Day",
        'last_year': "Month",
    }

    strftime_map = {
        'now': '%H:%M:%s',
        'last_day': "%H:%M",
        'last_week': "%d/%m",
        'last_month': "%d/%m",
        'last_year': "%b/%y",
    }

    dates = {
        'now': {
            'from': civis_format_time(now() - timedelta(minutes=15) - timedelta(days=2*365)),
            'to': civis_format_time(now() - timedelta(days=2*365)),
        },
        'last_day': {
            'from': civis_format_time(now() - timedelta(days=1) - timedelta(days=2*365)),
            'to': civis_format_time(now() - timedelta(days=2*365)),
            'res': 'hourly',
        },
        'last_week': {
            'from': civis_format_time(now() - timedelta(days=7) - timedelta(days=2*365)),
            'to': civis_format_time(now() - timedelta(days=2*365)),
            'res': 'daily',
        },
        'last_month': {
            'from': civis_format_time(now() - timedelta(days=30) - timedelta(days=2*365)),
            'to': civis_format_time(now() - timedelta(days=2*365)),
            'res': 'daily',
        },
        'last_year': {
            'from': civis_format_time(now() - timedelta(days=365) - timedelta(days=2*365)),
            'to': civis_format_time(now() - timedelta(days=2*365)),
            'res': 'monthly'
        },
    }

    params = {}
    params.update(dates[interval])
    params.update({
        'userID': user_id,
        'token': token,
        })

    print params

    consumption = requests.get(CIVIS_BASE_URL + "/downloadMyData", params=params)
    production = requests.get(
        CIVIS_BASE_URL + "/downloadMyData",
        params=dict(params, type='PV')
    )

    consumption = consumption.text
    production = production.text
    # with open('consumption.txt') as f:
    #     consumption = f.read()
    # with open('production.txt') as f:
    #     production = f.read()

    response = [
        [response_header_0_map[interval], "Consumo", "Produzione"],
    ]

    consumptions = OrderedDict()
    rtree = ElementTree.fromstring(consumption)

    for intervalReading in rtree:
        if intervalReading.tag != "IntervalReading":
            continue

        ts = value = None

        for el in intervalReading:
            if el.tag == "value":
                value = el.text
            if el.tag == "timePeriod":
                for el2 in el:
                    if el2.tag == "start":
                        ts = el2.text
        print ts, value
        consumptions[ts] = value

    productions = OrderedDict()
    rtree = ElementTree.fromstring(production)

    for intervalReading in rtree:
        if intervalReading.tag != "IntervalReading":
            continue

        ts = value = None

        for el in intervalReading:
            if el.tag == "value":
                value = el.text
            if el.tag == "timePeriod":
                for el2 in el:
                    if el2.tag == "start":
                        ts = el2.text
        print ts, value
        productions[ts] = value

    for ts in consumptions:
        date = datetime.strptime(ts, CIVIS_TIME_FORMAT)
        date_s = date.strftime(strftime_map[interval])
        response.append(
            [date_s, int(consumptions[ts]), int(productions[ts])]
        )

    print response
    return json.dumps(response)
    # with open('consumption.txt', 'w') as o:
    #     o.write(consumption.text)

    # with open('production.txt', 'w') as o:
    #     o.write(production.text)



@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='static/')

@route('/')
@view('views/index')
def index():
    return None

@route('/estimate')
@view('views/estimate')
def estimate():
    address = request.params['address']
    if address.lower() == "trento":
        table_for_estimate = [
            ["7700 €", "7/8 anni", "825 €", "3 kW", "Impianto fotovoltaico da 25 mq"],
            ["14130 €", "6 anni", "1650 €", "6 kW", "Impianto fotovoltaico da 50 mq"],
            ["19800 €", "5 anni", "2474 €", "9 kW", "Impianto fotovoltaico da 75 mq"],
        ]
    else:
        # sud italia
        table_for_estimate = [
            ["7700 €", "6 anni", "1100 €", "3 kW", "Impianto fotovoltaico da 25 mq"],
            ["14130 €", "5 anni", "2200 €", "6 kW", "Impianto fotovoltaico da 50 mq"],
            ["19800 €", "4 anni", "3300 €", "9 kW", "Impianto fotovoltaico da 75 mq"],
        ]

    # Sorry, not enough time to collect the data!

    data = {
        'address': address,
        'pronsumers_n': 5,
        'table_estimate': table_for_estimate,
    }

    return data

@post('/private_area')
@view('views/private_area')
def private_area():
    # Maybe another time
    # if not check_credentials(request.forms.get('username'), request.forms.get('password')):
    #     abort(401, "Sorry, access denied.")
    user_id = request.forms.get('username')
    try:
        user_id = int(user_id)
    except ValueError:
        abort(401, "Credenziali non corrette")

    token = fetch_token_for_user(user_id)

    data = {
        # Badness level: over 9000
        "userID": user_id,
        "token": token,
        "last_month_production": 1,
        "last_month_consumption": 2,
        "last_year_earnings": {'color': "green", 'value': "+80"},
        "last_month_earnings": {'color': "green", 'value': "+10"},
        "health": "92%",
        "karma": {"text": "=)", "color": "green"}
    }
    return data

if __name__ == "__main__":
    port = os.environ.get('PORT', 8080)
    run(
        host='0.0.0.0',
        port=port,
        debug=True,
        reloader=True)

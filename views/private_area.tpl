<html>
  <head>
    <title> </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="static/css/ie/html5shiv.js"></script><![endif]-->
    <script src="static/js/jquery.min.js"></script>
    <script src="static/js/jquery.dropotron.min.js"></script>
    <script src="static/js/jquery.scrollgress.min.js"></script>
    <script src="static/js/jquery.scrolly.min.js"></script>
    <script src="static/js/jquery.slidertron.min.js"></script>
    <script src="static/js/skel.min.js"></script>
    <script src="static/js/skel-layers.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="static/js/init.js"></script>
    <script type="text/javascript">

google.load("visualization", "1", {packages:["corechart"]});

$(document).ready(function() {
  $('#button-last-day').click(function(){
        $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'last_day'},
                dataType: 'json',
                success: function (data){
                    var data_second_char = google.visualization.arrayToDataTable(data);
                    var options_second = {
                      title: 'Bilancio energetico',
                      hAxis: {title: 'Periodo',  titleTextStyle: {color: '#333'}},
                      vAxis: {minValue: 0},
                    };
                    var chart_second = new google.visualization.AreaChart(document.getElementById('chart_production_consumption'));
                    chart_second.draw(data_second_char, options_second);
                }
            }

        )

  });
  $('#button-last-week').click(function(){
    $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'last_week'},
                dataType: 'json',
                success: function (data){
                    var data_second_char = google.visualization.arrayToDataTable(data);
                    var options_second = {
                      title: 'Bilancio energetico',
                      hAxis: {title: 'Periodo',  titleTextStyle: {color: '#333'}},
                      vAxis: {minValue: 0},
                    };
                    var chart_second = new google.visualization.AreaChart(document.getElementById('chart_production_consumption'));
                    chart_second.draw(data_second_char, options_second);
                }
            }

        )

  });
  $('#button-last-month').click(function(){
    $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'last_month'},
                dataType: 'json',
                success: function (data){
                    var data_second_char = google.visualization.arrayToDataTable(data);
                    var options_second = {
                      title: 'Bilancio energetico',
                      hAxis: {title: 'Periodo',  titleTextStyle: {color: '#333'}},
                      vAxis: {minValue: 0},
                    };
                    var chart_second = new google.visualization.AreaChart(document.getElementById('chart_production_consumption'));
                    chart_second.draw(data_second_char, options_second);
                }
            }

        )

  });
  $('#button-last-year').click(function(){
    $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'last_year'},
                dataType: 'json',
                success: function (data){
                    var data_second_char = google.visualization.arrayToDataTable(data);
                    var options_second = {
                      title: 'Bilancio energetico',
                      hAxis: {title: 'Periodo',  titleTextStyle: {color: '#333'}},
                      vAxis: {minValue: 0},
                    };
                    var chart_second = new google.visualization.AreaChart(document.getElementById('chart_production_consumption'));
                    chart_second.draw(data_second_char, options_second);
                }
            }

        )

  });
});



  // CHARTS

google.setOnLoadCallback(drawChart);
      function drawChart() {

        $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'now'},
                dataType: 'json',
                success: function (data){
                    var consumption = data[1][1];
                    var production = data[1][2];
                    var balance = production - consumption;
                    var data = google.visualization.arrayToDataTable([
                            ['Label', 'Value'],
                            ['Bilancio', balance],
                        ]);
                    var options = {
                      redFrom: -2000, redTo: -400,
                        yellowFrom: -400, yellowTo: +400,
                      greenFrom:400 , greenTo: +2000,
                        minorTicks: 5, min: -2000, max: +2000
                    };
                    var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            }

        )

        // // Second Chart
        $.ajax('http://localhost:8080/api/plantData', 
            {
                headers: {userID: '{{ userID }}', token: '{{ token }}'},
                method: 'GET',
                data: {interval: 'last_day'},
                dataType: 'json',
                success: function (data){
                    var data_second_char = google.visualization.arrayToDataTable(data);
                    var options_second = {
                      title: 'Bilancio Totale',
                      hAxis: {title: 'Time',  titleTextStyle: {color: '#333'}},
                      vAxis: {minValue: 0},
                    };
                    var chart_second = new google.visualization.AreaChart(document.getElementById('chart_production_consumption'));
                    chart_second.draw(data_second_char, options_second);
                }
            }

        )
    }

    </script>
    <noscript>
      <link rel="stylesheet" href="static/css/skel.css" />
      <link rel="stylesheet" href="static/css/style.css" />
      <link rel="stylesheet" href="static/css/style-xlarge.css" />
    </noscript>
    <!--[if lte IE 9]><link rel="stylesheet" href="static/css/ie/v9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="static/css/ie/v8.css" /><![endif]-->
  </head>
  <body class="landing">
      <section id="three" class="wrapper style1">
        <div class="container">
          <header class="major">
            <h2>Bilancio energetico</h2>
          </header>
          <div class="row">
            <div class="last-month instantaneous">
              <article class="box post">
                <p>
                  Dati raccolti nell'ultimo mese:
                </p>
                <p>Prodotti: <span>330 kWh</span></p>
                <p>Consumati: <span>190 kWh</span></p>
                <hr>
                <p>Bilancio mensile: <span style="color: green">+12€</span></p>
                <p>Bilancio annuale: <span style="color: green">+71€</span></p>
                <p>Karma: 😃 <span style="color:green">continua così!</span></p>
                <hr>
                <p>Salute impianto: <span>92%</span></p>
              </article>
            </div>
            <div class="last-month instantaneous">
              <article class="box post">
                <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['gauge']}]}"></script>
                <div id="chart_div"></div>
                
              </article>
            </div>
            <div class="production_consumption">
              <article class="box post">
                <div id="chart_production_consumption"></div>
                <form>
                  <input  type="button" name="last_day" value="Giornaliero" id="button-last-day" checked>
                  <input  type="button" name="last_week" value="Settimanale" id="button-last-week">
                  <input  type="button" name="last_month" value="Mensile" id="button-last-month">
                  <input  type="button" name="last_year" value="Annuale" id="button-last-year">
                </form>
              </article>
            </div>
          </div>
        </div>
      </section>


    <!-- Footer -->
      <footer id="footer">
        <ul class="icons">
          <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
          <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
          <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
          <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
          <li><a href="#" class="icon fa-envelope"><span class="label">Envelope</span></a></li>
        </ul>
        <ul class="menu">
          <li><a href="#">FAQ</a></li>
          <li><a href="#">Terms of Use</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <span class="copyright">
          &copy; Copyright Shikataganai. All rights reserved. Design by <a href="http://www.html5webtemplates.co.uk">Responsive Web Templates</a>
        </span>
      </footer>

  </body>
</html>

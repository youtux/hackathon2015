<html>
  <head>
    <title> </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="static/css/ie/html5shiv.js"></script><![endif]-->
    <script src="static/js/jquery.min.js"></script>
    <script src="static/js/jquery.dropotron.min.js"></script>
    <script src="static/js/jquery.scrollgress.min.js"></script>
    <script src="static/js/jquery.scrolly.min.js"></script>
    <script src="static/js/jquery.slidertron.min.js"></script>
    <script src="static/js/skel.min.js"></script>
    <script src="static/js/skel-layers.min.js"></script>
    <script src="static/js/init.js"></script>
    <noscript>
      <link rel="stylesheet" href="static/css/skel.css" />
      <link rel="stylesheet" href="static/css/style.css" />
      <link rel="stylesheet" href="static/css/style-xlarge.css" />
    </noscript>
    <!--[if lte IE 9]><link rel="stylesheet" href="static/css/ie/v9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="static/css/ie/v8.css" /><![endif]-->
  </head>
  <body class="landing">

    <!-- Banner -->
      <section id="banner">
        <div class="inner">
          <h2>Paghi ancora per la tua elettricità?</h2>
          <p>Unisciti alla nostra community!</p>
          <ul class="actions">
            <li><a href="#" class="button big scrolly" id="login-btn">Login</a></li>
            <li><a href="#" class="button big scrolly" id="estimate-btn">Che altre opzioni ho?</a></li>
          </ul>
          <form method="POST" action="private_area" id="login-form" style="display: none; width: 400px">
            <input type="text" name="username" placeholder="Username" id="login-form-username">
            <input type="password" name="password" placeholder="Password" id="login-form-password">
            <input type="submit" value="Login">
          </form>
          <form action="estimate" id="estimate-form" style="display: none; width: 400px">
            <input type="text" name="address" placeholder="Dove abiti?" id="estimate-form-city">
            <input type="submit" value="Vai">
          </form>
        </div>
      </section>

    

    <!-- Footer -->
      <footer id="footer">
        <ul class="icons">
          <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
          <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
          <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
          <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
          <li><a href="#" class="icon fa-envelope"><span class="label">Envelope</span></a></li>
        </ul>
        <ul class="menu">
          <li><a href="#">FAQ</a></li>
          <li><a href="#">Terms of Use</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <span class="copyright">
          &copy; Copyright Shikataganai. All rights reserved. Design by <a href="http://www.html5webtemplates.co.uk">Responsive Web Templates</a>
        </span>
      </footer>

  </body>
</html>

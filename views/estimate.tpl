<html>
	<head>
		<title> </title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="static/css/ie/html5shiv.js"></script><![endif]-->
		<script src="static/js/jquery.min.js"></script>
		<script src="static/js/jquery.dropotron.min.js"></script>
		<script src="static/js/jquery.scrollgress.min.js"></script>
		<script src="static/js/jquery.scrolly.min.js"></script>
		<script src="static/js/jquery.slidertron.min.js"></script>
		<script src="static/js/skel.min.js"></script>
		<script src="static/js/skel-layers.min.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script src="static/js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="static/css/skel.css" />
			<link rel="stylesheet" href="static/css/style.css" />
			<link rel="stylesheet" href="static/css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="static/css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="static/css/ie/v8.css" /><![endif]-->
	</head>
	<body class="landing">
			<section id="three" class="wrapper style1">
				<div class="container">
           <header class="major">
						<h2>Quanto spendo nella mia città?</h2>
					</header>
					<div class="row">
						<div style="width:100%">
							<article class="box post">
							<!-- <p>Inserisci la città:</p> -->
								<form action="estimate" id="estimate-form-search" style="width:100%">
									<input type="text" name="address" value="{{ address }}" placeholder="Città" id="estimate-form-city">
									<input type="submit" value="Vai" id="submit-form">
								</form>
							</article>
						</div>
					</div>
					<div class="row">
						<div style="width:100%">
							<article class="box post">
								<h3>A {{ address }} ci sono altri {{ pronsumers_n }} utenti che spendono meno di te!</h3>
								<p>Devi sapere che un impianto fotovoltaico o eolico può avere produzione diversa a seconda della posizione geografica. Abbiamo cercato per te le soluzioni più favorevoli nella zona di tuo interesse.</p>
							</article>
						</div>
					</div>
					<div class="row">
						<div style="width:100%">
							<article class="box post">
								<h3>Ecco le opzioni più vantaggiose per la tua località:</h3>
								<table>
									<tr>
										<th>Spesa iniziale</th>
										<th>Ammortizz. in</th>
										<th>Guadagno *</th>
										<th>Potenza nominale</th>
										<th>Note</th>
									</tr>
									% for row in table_estimate:
									<tr>
										% for col in row:
										<td>{{ col }}</td>
										% end
									</tr>
									% end
								</table>
								* Il guadagno si riferisce al periodo successivo all'ammortizzamento.
							</article>
							<a href="#" class="button big scrolly" style="background-color: lightgreen;">Prenota un appuntamento</a>
						</div>
					</div>
				</div>
			</section>


		<!-- Footer -->
			<footer id="footer">
				<ul class="icons">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
					<li><a href="#" class="icon fa-envelope"><span class="label">Envelope</span></a></li>
				</ul>
				<ul class="menu">
					<li><a href="#">FAQ</a></li>
					<li><a href="#">Terms of Use</a></li>
					<li><a href="#">Privacy</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<span class="copyright">
					&copy; Copyright Shikataganai. All rights reserved. Design by <a href="http://www.html5webtemplates.co.uk">Responsive Web Templates</a>
				</span>
			</footer>

	</body>
</html>

